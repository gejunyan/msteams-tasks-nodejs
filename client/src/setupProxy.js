const proxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(proxy('/auth/google', { target: 'http://localhost:3333' }));
  app.use(proxy('/api/*', { target: 'http://localhost:3333' }));
  app.use(proxy('/hello', { target: 'http://localhost:3333' }));
  app.use(proxy('/first', { target: 'http://localhost:3333' }));
  app.use(proxy('/second', { target: 'http://localhost:3333' }));
  app.use(proxy('/configure', { target: 'http://localhost:3333' }));
};
