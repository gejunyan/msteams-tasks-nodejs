import React, { Component, Fragment } from 'react';
import Form from 'react-jsonschema-form';
import { MDBDataTable, MDBContainer, MDBModal, MDBModalBody, MDBModalHeader, MDBBtn, MDBIcon } from "mdbreact";
import axios from 'axios';
import API from '../../utils/api_url';
import './ExternalCollectionPage.css';
import { Toast } from "../../utils/helperFunctions";
const apiUrl = API.API_URL;

class ExternalCollectionPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      modalStatus: false,
      collectionName: 'External Content',
      column: ['name', 'subject', 'description', 'created', 'status', 'workflow'],
      record: null,
      formSchema: { title: '', type: "object", properties: {} },
      uiSchema: {},
      actionMessage: ''
    }
  }

  toggle = () => {
    this.setState({
      modalStatus: !this.state.modalStatus
    });
  }

  getTableRows() {
    let rows = this.state.record && this.state.record.map((r, i) => ({
      name: r.name,
      subject: r.subject,
      description: r.description,
      created: r.created,
      status: r.status,
      workflow: r.workflow,
      action: <a onClick={e => this.handleEditRecord(i)}>
        <MDBIcon icon="envelope" />
      </a>
    }));
    return rows
  }

  render() {
    const {
      collectionName,
      column,
      record,
      formSchema,
      uiSchema,
      actionMessage
    } = this.state
    const data = {
      columns: [
        {
          label: 'Name',
          field: 'name',
        },
        {
          label: 'Subject',
          field: 'subject',
        },
        {
          label: 'Description',
          field: 'description',
        }, {
          label: 'Created',
          field: 'created',
        }, {
          label: 'Status',
          field: 'status',
        }, {
          label: 'Workflow',
          field: 'workflow',
        }, {
          label: '',
          field: 'action',
          sort: 'disabled'
        }
      ],
      rows: this.getTableRows(),
    }
    return (
      <Fragment>
        <MDBContainer style={{minHeight:"80vh"}}>
          <div className="panel">
            <div className="panel-heading">
              <div className='panel-title'>
                <h3>{collectionName ? collectionName : 'Collection'}</h3>
              </div>
            </div>
          </div>
          <div className="panel-block bg-white">
            <div className='w-full overflow-auto'>
              {!record &&
                <div className='no-data-label'>Please wait while data is loading....</div>
              }
              {record && !record.length &&
                <div className='no-data-label'>No Data Available....</div>
              }
              {record && record.length &&
                <MDBDataTable striped bordered hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data={data} />
              }
            </div>
          </div>
        </MDBContainer>
        <MDBModal isOpen={this.state.modalStatus} toggle={this.toggle} size="sm">
          <MDBModalHeader toggle={this.toggle}>Task</MDBModalHeader>
          <MDBModalBody>
            <Form
              uiSchema={uiSchema}
              schema={formSchema}
            >
              <div className="btn-action-container">
                <MDBBtn color="secondary" size="sm" onClick={this.onApprove}>Approve</MDBBtn>
                <MDBBtn color="primary" size="sm" onClick={this.onReject}>Reject</MDBBtn>
              </div>
            </Form>
          </MDBModalBody>
        </MDBModal>
        {/* <div id="modal-post-action-message" className="modal">
          <div className="modal-content">
            <h5 className="title"><strong>{actionMessage}</strong></h5>
            <span className="waves-effect waves-light btn btn-close" onClick={this.handleClickClose}>
              Close
            </span>
          </div>
        </div> */}
      </Fragment>
    )
  }

  componentWillMount() {
    this.loadExternalRecords()
  }

  componentDidMount() {
    // materialize css initialization
    //  M.AutoInit()
  }

  loadExternalRecords() {
    axios.get(`${apiUrl}/external-content`)
      .then(res => {
        const record = res.data.data.tasks
        // map keys in returned data to form schema properties
        let properties = Object.keys(record[0]).reduce((obj, key) => {
          return { ...obj, [key]: { type: 'string', title: key, default: '' } }
        }, {})
        // add comment field in the form schema
        properties.comment = { type: 'string', title: 'comment' }

        const schema = { ...this.state.formSchema, properties }

        this.setState({
          record,
          formSchema: schema
        })
      })
      .catch(err => console.log(err))
  }

  handleEditRecord = (index) => {
    const { formSchema, record } = this.state
    const selectedRecord = record[index]
    const properties = formSchema.properties

    // add default value to show fields value in the form based on selected record
    const newProperties = Object.keys(properties).reduce((obj, key) => {
      if (key !== 'comment') {
        return { ...obj, [key]: { ...properties[key], default: selectedRecord[key] } }
      } else {
        return { ...obj, [key]: { ...properties[key] } }
      }
    }, {})

    // set UI schema for those fields to read-only
    const uiSchema = Object.keys(properties).reduce((obj, key) => {
      if (key !== 'comment') {
        /* eslint-disable-next-line */
        return { ...obj, [key]: { ["ui:readonly"]: true } }
      } else {
        return obj
      }
    }, {})

    this.setState({
      formSchema: { ...formSchema, properties: newProperties },
      uiSchema,
      modalStatus: true //open modal
    })
  }

  onApprove = () => {
    const { formSchema } = this.state
    const id = formSchema.properties.id.default

    axios.patch(`${apiUrl}/external-content?task_id=${id}`, { outcome: 'Approve' })
      .then(res => {
        const { result } = res.data
        let actionMessage

        this.openModalPostActionMessage()

        if (result.error) {
          actionMessage = `Fail to approve task\n${result.error}`
        } else {
          actionMessage = 'The task has been approved'
        }
        Toast(actionMessage);
        this.loadExternalRecords();
        this.setState({ actionMessage : actionMessage , modalStatus : false });
      })
      .catch(e => console.error(e))
  }

  onReject = () => {
    const { formSchema } = this.state
    const id = formSchema.properties.id.default

    axios.patch(`${apiUrl}/external-content?task_id=${id}`, { outcome: 'Reject' })
      .then(res => {
        const { result } = res.data
        let actionMessage

        this.openModalPostActionMessage()

        if (result.error) {
          actionMessage = `Fail to reject task\n${result.error}`
        } else {
          actionMessage = 'The task has been rejected'
        }
        Toast(actionMessage);
        this.loadExternalRecords();
        this.setState({ actionMessage :actionMessage, modalStatus : false })
      })
      .catch(e => console.error(e))
  }

  openModalPostActionMessage = (message) => {
    const elem = document.getElementById('modal-post-action-message')
    // const modal = M.Modal.getInstance(elem)
    //  modal.open()
  }

  handleClickClose = () => {
    const elem1 = document.getElementById('modal-post-action-message')
    const elem2 = document.getElementById('modal-edit-record')
    //M.Modal.getInstance(elem1).close()
    // M.Modal.getInstance(elem2).close()

    this.loadExternalRecords()
  }
}

export default ExternalCollectionPage
