import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from './containers/App';
import './index.css';
//import App from './App';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import configureStore from './utils/configureStore';
import 'react-notifications/lib/notifications.css';
import './assets/css/style.css';
const store = configureStore();
//import * as serviceWorker from './serviceWorker';

//ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
ReactDOM.render(
  <Router basename="/">
      <Provider store={store}>
        <Route component={App} />
      </Provider>
  </Router>,
  document.querySelector('#root')
);
