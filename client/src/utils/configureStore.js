import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import reducers from '../reducers';
import thunk from 'redux-thunk';
import axios from './axiosService'

const configureStore = () => {

  const middlewares = [thunk.withExtraArgument()];

  if (process.env.NODE_ENV !== "production") {
    middlewares.push(
      createLogger({
        colors: {
          title: () => "inherit",
          prevState: () => "red",
          action: () => "#03A9F4",
          nextState: () => "#4CAF50",
          error: () => "#F20404"
        }
      })
    );
  }
  const store = createStore(reducers,applyMiddleware(...middlewares));
  
  const jwt = localStorage.access_token;
  if (jwt) {
    const user = JSON.parse(localStorage.user);
    axios.setAuthToken(jwt);
  }
  return store;
};

export default configureStore;
