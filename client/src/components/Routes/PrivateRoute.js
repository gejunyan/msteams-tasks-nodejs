import React from 'react'
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({isAuthenticated, level ,component: Component, ...rest }) =>{
   return  <Route {...rest} render={props => <Component {...props} />} />
} 


function mapStateToProps(state) {
    return {
      isAuthenticated: null,
      level :  null 
    }
  }

export default connect(mapStateToProps)(PrivateRoute);
