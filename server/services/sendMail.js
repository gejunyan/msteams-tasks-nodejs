
const nodemailer = require('nodemailer');
const smtpTransport = require("nodemailer-smtp-transport");
var config = require('../../config/index');
const mongoUtil = require('./mongoUtil');
var Company = require('./../models/company.js');
var utility = require('./utility');
// create reusable transporter object using the default SMTP transport

var transporter = function (host) {
    var hostname = "portal";
    //console.log("host :" + host);
    if (host && utility.IsTenant(host)) {
        hostname = utility.getHostName({ hostname: host })
    }
    console.log("Mail send Host Name :" + hostname)
    return new Promise((resolve, reject) => {
        getCompanyEmailSMTP({ hostname: hostname }, function (company_data) {
            console.log("1");
            console.log(company_data);
            if (company_data) {
                if (company_data.smtp_server && company_data.smtp_port && company_data.smtp_auth && company_data.smtp_fromMail) {
                    var smtp = {
                        smtp_server: company_data.smtp_server,
                        smtp_port: company_data.smtp_port,
                        smtp_auth: company_data.smtp_auth,
                        smtp_fromMail: company_data.smtp_fromMail
                    }

                    const Transport = nodemailer.createTransport(
                        smtpTransport({
                            service: 'smtp',
                            host: smtp.smtp_server, //app.taskngin.com',
                            port: smtp.port,
                            secureConnection: true,  //secureConnection: true, // true for 465, false for other ports
                            auth: smtp.smtp_auth
                        })
                    )

                    resolve(Transport)
                } else {
                    //console.log("Mail setup " + hostname + " is doesn't exist, after used portal host.");
                    getCompanyEmailSMTP({ hostname: "portal" }, function (company_data) {
                        console.log("2");
                        
                        console.log(company_data);
                        if (company_data) {
                            var smtp = {
                                smtp_server: company_data.smtp_server,
                                smtp_port: company_data.smtp_port,
                                smtp_auth: company_data.smtp_auth,
                                smtp_fromMail: company_data.smtp_fromMail
                            }

                            const Transport = nodemailer.createTransport(
                                smtpTransport({
                                    service: 'smtp',
                                    host: smtp.smtp_server, //app.taskngin.com',
                                    port: smtp.port,
                                    secureConnection: true,  //secureConnection: true, // true for 465, false for other ports
                                    auth: smtp.smtp_auth
                                })
                            )
                            resolve(Transport)
                        } else {
                            //console.log("Mail setup is doesn't exist");
                            resolve(false)
                        }
                    });
                }
            } else {
                //console.log("Mail setup is doesn't exist");
                resolve(false)
            }

        });
    });


}

var sendMail = function (mailOptions) {
    return new Promise((resolve, reject) => {
        var host = null
        if (mailOptions.host) {
            host = mailOptions.host;
            delete mailOptions.host;
        }
        transporter(host).then((nodemailersConfig) => {
            if (nodemailersConfig) {
                console.log(mailOptions)
                nodemailersConfig.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        console.log(error)
                        resolve(false);   
                    }else{
                        console.log(info);
                        resolve(true)
                    }
                });
            }
        });
    });
}

var TestSend_Email = function (done) {
    var mailOptions = {
        from: config.smtp.smtp_fromMail,
        to: 'anup.maurya484@gmail.com',
        subject: 'Sending Email using Node.js',
        text: 'That was easy!',
    };
    transporter().sendMail(mailOptions, function (error, info) {
        if (error) {
            done({ 'res': '1', 'msg': error });
        }
        else {
            done({ 'res': '0', 'msg': 'Email sent: ' + info.response, message_id: info.response.split('Ok ')[1] });
        }
        //console.log(old_config_smpt + ":" + JSON.stringify(old_config));
        //console.log(old_config_smpt + ":" + JSON.stringify(config.smtp));
        config.smtp = old_config ? old_config : config.smtp;
        old_config = null;
    });
}

var sendSignUpEmail = function (payload) {

    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    let mailOptions = {
        from: config.smtp.smtp_fromMail, // sender address
        to: payload.emailTo, // list of receivers
        subject: 'Registration Successfully Done.', // Subject line
        html: "<p><h3>Hi, " + payload.emailTo + " <h3></p><p>Yor are successfully registered </p><p></p>" // plain text body
    };

    // send mail with defined transport object
    sendMail(mailOptions)

}

var sendVerifyEmail = function (payload) {
    // setup email data with unicode symbols
    let mailOptions = {
        from: config.smtp.smtp_fromMail, // sender address
        to: payload.emailTo, // list of receivers
        subject: 'Registration Successfully Complated.', // Subject line
        html: "Hi, " + payload.name + " <p>Please verify your email address so we know that it's really you!<p><a  href= '" + "http://localhost:3000/login?verifycode=" + payload.verify_code + "' style='font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block'>Verify my email address</a>"
    };
    // send mail with defined transport object
    sendMail(mailOptions)
}

var forgetUserPassword = function (payload) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    // setup email data with unicode symbols
    console.log(payload.host)
    let mailOptions;
    if (payload.host) {
        mailOptions = {
            from: config.smtp.smtp_fromMail, // sender address
            to: payload.emailTo, // list of receivers
            subject: 'Password reset', // Subject line
            html: "Hi, " + payload.name + "<br/> <a  href= '" + payload.url + "' style='font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block'>Reset my password</a>",
            host: payload.host
        };
    } else {
        mailOptions = {
            from: config.smtp.smtp_fromMail, // sender address
            to: payload.emailTo, // list of receivers
            subject: 'Password reset', // Subject line
            html: "Hi, " + payload.name + "<br/> <a  href= '" + payload.url + "' style='font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block'>Reset my password</a>"
        };
    }

    // send mail with defined transport object
    sendMail(mailOptions)
}

var resetNewUserPassword = function (payload) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    // setup email data with unicode symbols
    let mailOptions = {
        from: config.smtp.smtp_fromMail, // sender address
        to: payload.emailTo, // list of receivers
        subject: 'Add New Password', // Subject line
        html: "Hi, " + payload.name + "<br/> <a  href= '" + payload.url + "' style='font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block'>Reset my password</a>"

    };

    // send mail with defined transport object
    sendMail(mailOptions)
}

var resetTenantUserPassword = function (payload) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    // setup email data with unicode symbols
    let mailOptions = {
        from: config.smtp.smtp_fromMail, // sender address
        to: payload.emailTo, // list of receivers
        subject: 'Add New Password', // Subject line
        html: "Hi, " + payload.name + "<br/> <a  href= '" + payload.url + "' style='font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:11px solid #3572b0;border-bottom:11px solid #3572b0;border-left:20px solid #3572b0;border-right:20px solid #3572b0;border-radius:5px;display:inline-block'>Reset my password</a>",
        host: payload.host ? payload.host : null
    };
    // send mail with defined transport object
    sendMail(mailOptions)
}

var getCompanyEmailSMTP = function (payload, done) {
    Company.findOne(payload).exec(function (err, company_data) {
        if (!err) {
            if (company_data) {
                done(company_data);
            } else {
                done(false);
            }
        } else {
            done(false);
        }
    });
}

var SendSurveyInvitation = function (payload, done) {
    console.log(payload.link)
    let mailOptions = {
        from: config.smtp.smtp_fromMail, // sender address
        to: payload.emailTo, // list of receivers
        subject: payload.subject, // Subject line
        html: '<div style="background-color: #f5f5f5;padding: 30px;text-align: center;"><h5 style="font-size: 1.64rem;line-height: 110%;margin: 1.0933333333rem 0 .656rem 0;">'+payload.surve_name+'</h5><p>We re running a survey and whould love your input. Please follow below to respond to the survey. Thank you for your participationt</p><a style="color: #039be5;text-decoration: none;-webkit-tap-highlight-color: transparent;" href="'+payload.link+'" target="_blank">'+
        '<button type="button" style="text-decoration: none;color: #fff;background-color: #26a69a;text-align: center;letter-spacing: .5px;-webkit-transition: background-color .2s ease-out;transition: background-color .2s ease-out;'+
        'cursor: pointer;border: none;border-radius: 2px;display: inline-block;height: 36px;line-height: 36px;padding: 0 16px;text-transform: uppercase;vertical-align: middle;-webkit-tap-highlight-color: t;">Begin Survey</button></a></div>'

    };
    // send mail with defined transport object
    sendMail(mailOptions).then(data=>{
        done(true);
    });

}

exports.sendSignUpEmail = sendSignUpEmail;
exports.sendVerifyEmail = sendVerifyEmail;
exports.forgetUserPassword = forgetUserPassword;
exports.TestSend_Email = TestSend_Email;
exports.resetNewUserPassword = resetNewUserPassword;
exports.resetTenantUserPassword = resetTenantUserPassword;
exports.SendSurveyInvitation = SendSurveyInvitation;
