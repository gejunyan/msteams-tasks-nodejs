const passport = require('passport')
const GoogleStrategy = require('passport-google-oauth20').Strategy
const mongoUtil = require('./mongoUtil')
const keys = require('../../config/keys')
var utility = require('../services/utility');

passport.use(
  new GoogleStrategy(
    {
      clientID: keys.googleClientID,
      clientSecret: keys.googleClientSecret,
      callbackURL: '/auth/google/callback',
      proxy: true
    },
    (accessToken, refreshToken, profile, done) => {


      var payload = {
        name: profile.displayName,
        email: profile.emails[0].value,
        googleId: profile.id
      }

      const User = mongoUtil.getDB().collection('users')
      User.findOne({ googleId: profile.id }, (err, user) => {
        if (err) console.error(err)
        if (user) return done(null, user)
        else {

          var userarr = {
            firstname: payload.name.split(" ")[0],
            lastname: payload.name.split(" ")[1],
            email: payload.email,
            password: utility.encrypt(Math.random().toString(36).substring(2)),
            level: 2,
            login_type: 2,
            googleId: payload.googleId,
            credits: 0,
            is_verify: true,
            is_deleted: false,
            is_active: true,
          }
          User.findOne({ email: payload.email }, (err, user) => {
            if (err) console.error(err)
            if (user) {
              User.updateOne({ email: payload.email },
                { $set: { googleId: payload.googleId } },
                (err, obj) => {
                  if (err) {
                    console.error(err)
                  }
                  else {
                    if (obj.result.ok) {
                      User.findOne({ googleId: payload.googleId }, (err, user) => {
                        done(null, user)
                      })
                    }
                  }
                })
            } else {
              User.insertOne(userarr, (err, obj) => {
                if (obj.result.ok) done(null, obj.ops[0])
              })
            }
          })

        }
      })
    }
  )
)

passport.serializeUser((user, done) => {
  done(null, user.googleId)
})

passport.deserializeUser((googleId, done) => {
  if(mongoUtil.getDB()){
    const User = mongoUtil.getDB().collection('users')
    User.findOne({ googleId }, (err, user) => {
      done(err, user)
    })
  }
})