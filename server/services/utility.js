const crypto = require("crypto");

const algorithm = 'aes-256-ctr';
const password = 'n^6F!423DCvOlSfs*6!55wR2X4w20ji8';

var encrypt = function (text) {
    var cipher = crypto.createCipher(algorithm, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

var decrypt = function (text) {
    var decipher = crypto.createDecipher(algorithm, password);
    try {
        var dec = decipher.update(text, 'hex', 'utf8');
        dec += decipher.final('utf8');
        return dec;
    }
    catch (err) {
        return "";
    }
}

var getHostInfo = function (req) {
    var host = req.hostname.split(".")[0];
    let user_emaildomain = "", user_hostname = "";
    if (req.body && req.body.email) {
        user_emaildomain = req.body.email.split("@")[1];
        user_hostname = user_emaildomain.split(".")[0];
    }
    var temp_data = {
        hostName: host,
        emailDomain: host + ".com",
        request_emaildomain: user_emaildomain,
        request_hostname: user_hostname
    }
    return temp_data
}


var getHostName = function (req) {
    try{
        var host_name = req.hostname.split(".")[0];
    }catch(e){
        host_name = req.hostname;
    }
    
    return host_name
}

var IsTenant = (host) => {
    var url = host.split(".")[0];
    var result = true;
    if ((url == "localhost") || (url == "portal") || (url=="flowngin")) {
        result = false
    }
    return result;
}

var IsProduction = (host) =>{
    //console.log(host);
    return (host.search("localhost")>=0) ? false : true
}

exports.encrypt = encrypt
exports.decrypt = decrypt
exports.getHostInfo = getHostInfo  
exports.getHostName = getHostName
exports.IsTenant = IsTenant
exports.IsProduction = IsProduction



